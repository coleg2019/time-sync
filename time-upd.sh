#!/bin/bash
#
#install ntpdate

if [ $(dpkg-query -W -f='${Status}' ntpdate 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install ntpdate;
fi

#update time with server ntp.time.in.ua
sudo ntpdate -u ntp.time.in.ua
#start script every 5 minutes
crontab -e
*/5 * * * * sh /home/oleg/time-sync/time-upd.sh >/dev/null 2>&1
