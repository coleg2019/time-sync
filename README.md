##
# clone files with repository
git clone https://coleg2019@bitbucket.org/coleg2019/time-sync.git
# if have been changed any files then update all files with repo to my server
git pull origin master
# change permission to exec script
chmod +x /home/oleg/time-sync/time-upd.sh
# start script
sh /home/oleg/time-sync/time-upd.sh
# add this script to crontab
crontab -e
# run script every 5 minute
*/5 * * * * /home/oleg/time-sync/time-upd.sh
